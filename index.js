class ShoppingCart {
  constructor(quantity, dateAdded) {
    this._cartId = "";
    this._productId = "";
    this.quantity = quantity;
    this.dateAdded = dateAdded;
  }
  set cartId(val) {
    if (isNaN(val)) {
      throw new Error("Invalid type");
    }
    this._cartId = val;
  }
  get cartId() {
    return this._cartId;
  }
  set productId(val) {
    if (isNaN(val)) {
      throw new Error("Invalid type");
    }
    this._productId = val;
  }
  get productId() {
    return this._productId;
  }

  addCartItem() {}
  updateQuantity() {}
  viewCartDetails() {}
  checkOut() {}
}

const cartOne = new ShoppingCart("21", "23.06.2021");
try {
  cartOne.cartId = "33";
} catch (e) {
  console.log(e.message);
}
try {
  cartOne.productId = "34";
} catch (e) {
  console.log(e.message);
}
console.log(cartOne);

class Customer {
  constructor(adress, email, creditCardInfo, shippingInfo) {
    this._costumerName = "";
    this.adress = adress;
    this.email = email;
    this.creditCardInfo = creditCardInfo;
    this.shippingInfo = shippingInfo;
    this._accountBallance = "";
  }
  set costumerName(val) {
    if (Number(val)) {
      throw new Error("Invalid type");
    }
    this._costumerName = val;
  }
  get costumerName() {
    return this._costumerName;
  }
  set accountBallance(val) {
    if (isNaN(val)) {
      throw new Error("Invalid type");
    }
    this._accountBallance = val;
  }
  get accountBallance() {
    return this._accountBallance;
  }
  register() {}
  login() {}
  updateProfile() {}
}
const costumer = new Customer(
  "Minsk",
  "fdsdf@fsdf.df",
  "visa mastercard gold platinum",
  "und"
);
try {
  costumer.costumerName = "666";
} catch (e) {
  console.log(e.message);
}
try {
  costumer.accountBallance = "fui";
} catch (e) {
  console.log(e.message);
}
console.log(costumer);

class Order {
  constructor(dateCreated, dateShipped, costumerName, costumerId, shippingId) {
    this._orderId = "";
    this.dateCreated = dateCreated;
    this.dateShipped = dateShipped;
    this.costumerName = costumerName;
    this.costumerId = costumerId;
    this._status = "";
    this.shippingId = shippingId;
  }
  set orderId(val) {
    if (isNaN(val)) {
      throw new Error("Invalid type");
    } else if (val < 0 || val > 100) {
      throw new Error("Invalid id number");
    }
    this._orderId = val;
  }
  get orderId() {
    return this._orderId;
  }
  set status(val) {
    if (val !== "complete" || val !== "process") {
      throw new Error("Wrong status");
    }
    this._status = val;
  }
  get status() {
    return this._status;
  }
  placeOrder() {}
}
const order = new Order("21.06.2021", "24.06.2021", "Mr.X", "44", "78");
try {
  order.orderId = "555";
} catch (e) {
  console.log(e.message);
}
try {
  order.status = "approve";
} catch (e) {
  console.log(e.message);
}
console.log(order);

class ShippingInfo {
  constructor(shippingId, shippingConst, shippingRegionId) {
    this.shippingId = shippingId;
    this._shippingType = "";
    this.shippingConst = shippingConst;
    this.shippingRegionId = shippingRegionId;
  }
  set shippingType(val) {
    if (val !== "air" || val !== "boat") {
      throw new Error("Wrong shipping type");
    }
    this._status = val;
  }
  get status() {
    return this._status;
  }
  updateShippingInfo() {}
}
const shipping = new ShippingInfo("55", "777", "25");
try {
  shipping.shippingType = "train";
} catch (e) {
  console.log(e.message);
}
console.log(shipping);

class User {
  constructor(userId, loginStatus) {
    this.userId = userId;
    this._password = "";
    this.loginStatus = loginStatus;
    this._registerDate = "";
  }
  set registerDate(val) {
    if (!Date.parse(val)) {
      throw new Error("Wrong date form");
    }
    this._registerDate = val;
  }
  get registerDate() {
    return this._registerDate;
  }
  set password(val) {
    if (val.length < 10) {
      throw new Error("Too short password");
    }
    this._password = val;
  }
  get password() {
    return this._password;
  }
  veryfyLogin() {}
}

const user = new User("12", "login");
try {
  user.password = "пкку65ы6";
} catch (e) {
  console.log(e.message);
}
try {
  user.registerDate = "04.32.2021";
} catch (e) {
  console.log(e.message);
}
console.log(user);

class Administrator {
  constructor(adminName) {
    this.adminName = adminName;
    this._email = "";
  }
  set email(val) {
    let s = /\S+@\S+\.\S+/;
    if (!s.test(val)) {
      throw new Error("Wrong email adress");
    }
    this._email = val;
  }
  get email() {
    return this._email;
  }
  updateCatalog() {}
}
const admin = new Administrator("Alex");
try {
  admin.email = "gggg@mmmee";
} catch (e) {
  console.log(e.message);
}
console.log(admin);

class OrderDetails {
  constructor(orderId, productId, productName, unitCost, subTotal) {
    this.orderId = orderId;
    this.productId = productId;
    this.productName = productName;
    this._quantity = "";
    this.unitCost = unitCost;
    this.subTotal = subTotal;
  }

  set quantity(val) {
    if (val < 1) {
      throw new Error("Wrong quantity");
    }
    this._quantity = val;
  }
  get quantity() {
    return this._quantity;
  }

  calcPrice() {}
}

const orderDetails = new OrderDetails("12", "44", "phone", "467", "98");
try {
  orderDetails.quantity = "0";
} catch (e) {
  console.log(e.message);
}
console.log(orderDetails);
